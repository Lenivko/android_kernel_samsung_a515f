#!/bin/bash

export RED='\033[0;31m'
export GREEN='\033[0;32m'
export PURPLE='\033[1;35m'
export NC='\033[0m'
export BLUE='\033[0;34m'

#ccache
export USE_CCACHE=1
ccache -M 50G
export PATH=$PATH:~/a515f/aarch64-cruel-elf/bin

echo -e "${RED}Scenix kernel for Samsung Galaxy A51"
echo -e "${GREEN}"
PS3='Enter your choice: '
options=("Build" "Clean" "Edit config (OneUI)" "Edit config (GSI)" "Quit")
select opt in "${options[@]}"
do
    case $opt in
        "Build")
            echo -e "${PURPLE}"
            #OneUI
            echo "Building for OneUI..."
            mkdir -p scenix/out_oneui
            make O=scenix/out_oneui -j$(nproc) ARCH=arm64 scnx_a51_defconfig > /dev/null 2>&1
            make O=scenix/out_oneui -j$(nproc) ARCH=arm64 \
            CROSS_COMPILE=~/a515f/aarch64-cruel-elf/bin-ccache/aarch64-cruel-elf- 2>&1 | tee -a scenix/build_oneui.log > /dev/null
            cp scenix/out_oneui/arch/arm64/boot/Image scenix/AK_oneui/Image
            cp scenix/out_oneui/arch/arm64/boot/dts/exynos/exynos9610.dtb scenix/AK_oneui/dtb_empty
            ./scenix/mkdtboimg cfg_create --dtb-dir=scenix/out_oneui/arch/arm64/boot/dts/samsung scenix/AK_oneui/dtbo scenix/dtbo.cfg
            cd scenix/AK_oneui
            cp ../dtb_header ./dtb
            cat dtb_empty >> dtb
            rm ../*OneUI*.zip
            ./zip.sh
            mv *.zip ../

            echo -e "${BLUE}"
            #GSI
            echo "Building for GSI..."
            cd ../../
            mkdir -p scenix/out_gsi
            make O=scenix/out_gsi -j$(nproc) ARCH=arm64 scnx_a51_gsi_defconfig > /dev/null 2>&1
            make O=scenix/out_gsi -j$(nproc) ARCH=arm64 \
            CROSS_COMPILE=~/a515f/aarch64-cruel-elf/bin-ccache/aarch64-cruel-elf- 2>&1 | tee -a scenix/build_gsi.log > /dev/null
            cp scenix/out_gsi/arch/arm64/boot/Image scenix/AK_gsi/Image
            cp scenix/out_gsi/arch/arm64/boot/dts/exynos/exynos9610.dtb scenix/AK_gsi/dtb_empty
            ./scenix/mkdtboimg cfg_create --dtb-dir=scenix/out_gsi/arch/arm64/boot/dts/samsung scenix/AK_gsi/dtbo scenix/dtbo.cfg
            cd scenix/AK_gsi
            cp ../dtb_header ./dtb
            cat dtb_empty >> dtb
            rm ../*GSI*.zip
            ./zip.sh
            mv *.zip ../
            break
            ;;
        "Clean")
            echo -e "${PURPLE}"
            make O=scenix/out_gsi mrproper
            make O=scenix/out_oneui mrproper
            break
            ;;
        "Edit config (OneUI)")
            echo -e "${PURPLE}"
            mkdir -p scenix/out_oneui
            make O=scenix/out_oneui ARCH=arm64 scnx_a51_defconfig
            make O=scenix/out_oneui ARCH=arm64 menuconfig
            cp scenix/out_oneui/.config arch/arm64/configs/scnx_a51_defconfig
            break
            ;;
        "Edit config (GSI)")
            echo -e "${BLUE}"
            mkdir -p scenix/out_gsi
            make O=scenix/out_gsi ARCH=arm64 scnx_a51_gsi_defconfig
            make O=scenix/out_gsi ARCH=arm64 menuconfig
            cp scenix/out_gsi/.config arch/arm64/configs/scnx_a51_gsi_defconfig
            break
            ;;
        "Quit")
            break
            ;;
        *) echo "invalid option $REPLY";;
    esac
done
echo -e "${NC}"